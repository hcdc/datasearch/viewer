import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TimerangeselectComponent } from './timerangeselect.component';

describe('TimerangeselectComponent', () => {
  let component: TimerangeselectComponent;
  let fixture: ComponentFixture<TimerangeselectComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TimerangeselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerangeselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
