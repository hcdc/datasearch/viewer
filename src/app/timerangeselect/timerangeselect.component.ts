import { DateService } from './../services/date.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-timerangeselect',
  templateUrl: './timerangeselect.component.html',
  styleUrls: ['./timerangeselect.component.css']
})
export class TimerangeselectComponent {
  constructor(public dateService: DateService) {
  }

}
