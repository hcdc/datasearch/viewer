import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CollapsecontainerComponent } from './collapsecontainer.component';

describe('CollapsecontainerComponent', () => {
  let component: CollapsecontainerComponent;
  let fixture: ComponentFixture<CollapsecontainerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CollapsecontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsecontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
