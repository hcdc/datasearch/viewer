import { Component, AfterViewInit, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { NONE_TYPE } from '@angular/compiler';
import { Collapsible } from '../services/collapsible';




@Component({
  selector: 'app-collapsecontainer',
  templateUrl: './collapsecontainer.component.html',
  styleUrls: ['./collapsecontainer.component.css'],
  animations: [
    trigger('smoothCollapse', [
      state('initial', style({
        display:'none',
        overflow:'hidden',
        opacity:'0',
        margin:0,
        height:0,
        padding: 0
      })),
      state('final', style({
        opacity:'1',
        overflow:'visible'
      })),
      transition('initial=>final', animate('500ms')),
      transition('final=>initial', animate('500ms'))
    ]),
  ]
})
/**
 * a container that should be used to wrap
 * all major elements like Filter/Result etc.
 * This way a consistant look and behavior can be achieved
 * Minimizing/maximizing can be controlled by a collapse control
 */
export class CollapsecontainerComponent implements AfterViewInit {
  @Input() service: Collapsible;
  @Input() contentComponent: any;

  @ViewChild('contentComponent', { read: ViewContainerRef }) contentComponentVCR: any;

  constructor(private componentResolver: ComponentFactoryResolver) {}

  ngAfterViewInit():void {
    const factory2 = this.componentResolver.resolveComponentFactory(this.contentComponent);
    const ref2 = this.contentComponentVCR.createComponent(factory2);
    ref2.changeDetectorRef.detectChanges();
  }
}
