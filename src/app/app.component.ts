import { ResultComponent } from './result/result.component';
import { ResultService } from './services/result.service';
import { CartComponent } from './cart/cart.component';
import { CartService } from './services/cart.service';
import { FilterComponent } from './filter/filter.component';
import { MapService } from './services/map.service';
import { FilterService } from './services/filter.service';
import { DateService } from './services/date.service';
import { Component } from '@angular/core';
import { MapWrapperComponent } from './mapwrapper/mapwrapper.component';
import { TimerangeselectComponent } from './timerangeselect/timerangeselect.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public dateService: DateService,
    public filterService: FilterService,
    public mapService: MapService,
    public cartService: CartService,
    public resultService: ResultService,
    ) {}
    timerangeselectComponent:any =  TimerangeselectComponent;
    filterComponent:any = FilterComponent;
    mapComponent:any = MapWrapperComponent;
    cartComponent:any = CartComponent;
    resultComponent: any = ResultComponent;
}
