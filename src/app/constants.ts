import { InjectionToken } from '@angular/core';

export const DATA = new InjectionToken<any>('component type');

export const SINGULAR_NAMES = {
    PROJECTS: 'PROJECT',
    TAGS: 'TAG',
    NAMES: 'NAME',
    PLATFORMS: 'PLATFORM'
};

export const PLURAL_NAMES = {
    CAMPAIGN: 'CAMPAIGNS',
    PROJECT: 'PROJECTS',
    TAG: 'TAGS',
    NAME: 'NAMES',
    PLATFORM: 'PLATFORMS'
};


export const TYPETOCSSCLASS = {
    CAMPAIGN: 'fas fa-ship',
    PARAMETER: 'fas fa-flask',
    NAME: 'fas fa-users',
    PROJECT: 'fas fa-ship',
    PLATFORM: 'fas fa-luggage-cart',
    TAG: 'fas fa-tags',
    PARAMETER_STANDARDNAME: 'fas fa-flask',
    VESSEL: 'fas fa-ship',
    PARAMETERSITE: 'fa fa-globe',
    PARAMETER_LONGNAME: 'fas fa-flask',
    PLATFORMTYPE: 'fas fa-satellite-dish',
    SCIENTIST: 'fas fa-users',
    DATASET: 'fas fa-clipboard',
    INSTITUTE: 'fa fa-university',
    TEMPORALRESOLUTION: 'fas fa-clock',
    TITLE: 'fa fa-text-width',
};

export const RESULTEXPLANATION_PREFIX = 'Each Result corresponts to an existing combination of a';
export const RESULTEXPLANATION_PREFIX_ALT = 'Following Measurements';
