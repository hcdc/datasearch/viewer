import { ResultService } from './../services/result.service';
import { MapService } from './../services/map.service';
import { DateService } from './../services/date.service';
import { FilterService } from './../services/filter.service';
import { UtilService } from '../services/util.service';

import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnDestroy{
  coords: string;
  timerange: string;
  mapSubscription: Subscription;
  dateSubscription: Subscription;
  constructor(
    public resultService: ResultService,
    public filterService: FilterService,
    public dateService: DateService,
    public mapService: MapService,
    private utilService: UtilService,
    ) {
        this.mapSubscription = mapService.coordsBS$.subscribe({
          next: coords =>{
              this.coords = !coords.is_geopoint ? `Longitude: ${this.utilService.round(coords.bbox.minlon)}, ${this.utilService.round(coords.bbox.maxlon)}, Latitude: ${this.utilService.round(coords.bbox.minlat)}, ${this.utilService.round(coords.bbox.maxlat)}`
              :
                `LAT: ${this.utilService.round(coords.point.lat)}, LON: ${this.utilService.round(coords.point.lon)}, Radius: ${coords.point.radius} ${coords.point.radiusUnit}`
              ;
          }
        });
        this.dateSubscription = dateService.timeRangeBS$.subscribe({
          next: range =>{
            this.timerange =  dateService.pretty(range);
          }
        })
    }
    ngOnDestroy() {
      this.mapSubscription.unsubscribe();
      this.dateSubscription.unsubscribe();
    }
}
