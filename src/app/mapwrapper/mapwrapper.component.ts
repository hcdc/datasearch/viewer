import { Component } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { MapService } from '../services/map.service';

@Component({
  selector: 'app-mapwrapper',
  templateUrl: './mapwrapper.component.html',
  styleUrls: ['./mapwrapper.component.css']
})
export class MapWrapperComponent {
   // Set our map properties
  mapCenter = [6.1278, 55.5074];
  basemapType = 'oceans';
  mapZoomLevel = 5;
  mapMinZoomLevel = 3;
  mapMaxZoomLevel = 12;
  constructor(public mapService: MapService) {}

}
