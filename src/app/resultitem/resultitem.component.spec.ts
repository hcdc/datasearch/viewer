import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResultitemComponent } from './resultitem.component';

describe('ResultitemComponent', () => {
  let component: ResultitemComponent;
  let fixture: ComponentFixture<ResultitemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
