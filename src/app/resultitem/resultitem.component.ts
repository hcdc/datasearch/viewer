import { CartService } from './../services/cart.service';
import { UtilService } from '../services/util.service';
import { FilterService } from '../services/filter.service';
import { Component, OnInit, Input } from '@angular/core';
import { Hit } from '../typings';
import { ResultService } from '../services/result.service';
import { DateService } from '../services/date.service';
import { MapService } from '../services/map.service';

@Component({
  selector: 'app-resultitem',
  templateUrl: './resultitem.component.html',
  styleUrls: ['./resultitem.component.css']
})
export class ResultitemComponent implements OnInit {
  @Input() hit: Hit;
  @Input() resultIndex: number;
  @Input() isInCart: boolean;
  @Input() id: number;
  listings = [];
  dataPayload = [];
  dataMeta = [];
  chosenFormats = ['csv'];
  options = [];
  IsDownloadable: boolean = true;
  private extra_filter_meta = "";
  ngOnInit(): void {
    this.dataPayload = this.utilService.objToArrayOfAttr(this.hit.payload);
    // add label to payload
    this.dataPayload.forEach(payload => {
      // JSON.parse(JSON.stringify(payload));
      const input = this.utilService.objToArrayOfAttr(payload.input);
      payload.input = input[0].input;
      payload.label = input[1].input;
      if (input.length > 2) payload.meta = input[2].input;
    })
    this.dataMeta = this.filterMeta(this.utilService.objToArrayOfAttr(this.hit.meta));
    this.dataMeta.find(m => {
      if (m.type == "Coordinates") {
        if (m.input.point || m.input.bbox) {
          m.input = !m.input.is_geopoint ? `Longitude: [${this.utilService.round(m.input.bbox.minlon)}, ${this.utilService.round(m.input.bbox.maxlon)}], Latitude: [${this.utilService.round(m.input.bbox.minlat)}, ${this.utilService.round(m.input.bbox.maxlat)}]`
            :
            `LAT: ${m.input.point.lat}, LON: ${m.input.point.lon}, Radius: ${m.input.point.radius} ${m.input.point.radiusUnit}`
            ;
        }
        return
      }
    });
    const di = this.dataMeta.indexOf(this.dataMeta.find(m => m.type == "download"));
    this.IsDownloadable = (di >= 0) ? this.dataMeta[di].input.downloadable: true;
    this.dataMeta.push(this.dataMeta.splice(this.dataMeta.indexOf(this.dataMeta.find(m => m.type == "Coordinates")), 1)[0]);
    this.options = this.hit.downloadFormats;
  }
  constructor(
    public mapService: MapService,
    public dateService: DateService,
    private utilService: UtilService,
    public resultService: ResultService,
    public filterService: FilterService,
    public cartService: CartService) {
  }
  isValid(){
    return this.hit.downloadFormats && this.hit.downloadFormats.length > 0 && this.chosenFormats.length > 0;
  }
  filterMeta(meta): []{
    meta = meta.filter(field => {
      // show all meta fields in Cart
      if (this.isInCart) return field.type !== 'index';
      // do not show Date in Results
      return field.type !== 'Coordinates' && field.type !== 'index'
    });
    return meta;
  }
}
