import { CartService } from './../services/cart.service';
import { UtilService } from './../services/util.service';
import { DateService } from './../services/date.service';
import { MapService } from './../services/map.service';
import { FilterService } from './../services/filter.service';
import { Component } from '@angular/core';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent {
  constructor(
    public filterService: FilterService,
    public mapService: MapService,
    public dateService: DateService,
    public utilService: UtilService) { }

  clearAll(){
    this.filterService.clear();
    this.mapService.clear();
    this.dateService.resetBoth();
  }
}
