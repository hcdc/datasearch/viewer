/*
  Copyright 2019 Esri
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  loadModules
} from 'esri-loader';

import esri = __esri; // Esri TypeScript Types


import {
  MapService
} from '../services/map.service';

import {
  fromEvent
} from 'rxjs';

import {
  debounceTime
} from 'rxjs/operators';

import {
  filter
} from 'rxjs/internal/operators/filter';

@Component({
  selector: 'app-esri-map',
  templateUrl: './esri-map.component.html',
  styleUrls: ['./esri-map.component.scss']
})
export class EsriMapComponent implements OnInit {

  @Output() mapLoadedEvent = new EventEmitter < boolean > ();

  // maxlat input element
  @ViewChild('maxlatEl') maxlatEl: ElementRef;
  // minlat input element
  @ViewChild('minlatEl') minlatEl: ElementRef;
  // maxlon input element
  @ViewChild('maxlonEl') maxlonEl: ElementRef;
  // minlon input element
  @ViewChild('minlonEl') minlonEl: ElementRef;
  // lat input element
  @ViewChild('latEl') latEl: ElementRef;
  // lon input element
  @ViewChild('lonEl') lonEl: ElementRef;
  // radius input element
  @ViewChild('radiusEl') radiusEl: ElementRef;


  // The <div> where we will place the map
  @ViewChild('mapViewNode', {
    static: true
  }) mapViewEl: ElementRef;

  /**
   * _zoom sets map zoom
   * _center sets map center
   * _basemap sets type of map
   * _loaded provides map loaded status
   */
  _zoom;
  _minzoom;
  _maxzoom;
  _center;
  _basemap;
  _loaded = false;
  _mapView;
  _drawing = false;
  _show_coordinates = false;
  _fillSymbol;
  _addRectangle;
  // set activate circle
  public readonly activate_Circle = true;
  public readonly Circle_2_Rectangle = true; // switch circle shape to rectangle
  public readonly trash_title = "Clear map";
  public readonly rectangle_title = "Select rectangular region: Click and drag to draw rectangle";
  public readonly circle_title = "Select area around position: Click to select point, then drag to draw distance";
  public readonly apply_title = "Apply the new changes on the map";
  // default is bbox
  is_geopoint = false;
  input_changed_Ev = false;
  private _Sketch;
  private _fullscreen;
  private location;
  private _centerGraphic;
  private _edgeGraphic;
  private _polylineGraphic;
  private _polylineAzimuth = 90;
  private _lon_start;
  private _lat_start;
  private _lon_end;
  private _lat_end;
  private _labelGraphic;
  private _vertices;
  private _start_point;
  private _end_point;
  private _polyline;
  private _pointSymbol;
  private _polylineSymbol;
  private _Graphic_func;
  private _Point_func;
  private _webMercatorUtils_func;
  private _Polyline_func;
  private _geometryEngine_func;
  private _geodesicUtils_func;
  private _Polygon_func;
  private _Circle_func;
  private _Extent_func;
  private _radius_scale;
  private earth_radius_km = 6371;
  private min_radius_km = 0.01;

  @Input() maxlat: number;
  @Input() minlat: number;
  @Input() maxlon: number;
  @Input() minlon: number;
  @Input() notification_area: string = "";
  @Input() lat: number;
  @Input() lon: number;
  @Input() radius: number;
  @Input() radiusUnit: string = "kilometers";

  private container_temp : object = undefined;

  get mapLoaded(): boolean {
    return this._loaded;
  }

  @Input()
  set zoom(zoom: number) {
    this._zoom = zoom;
  }

  get zoom(): number {
    return this._zoom;
  }

  @Input()
  set center(center: Array < number > ) {
    this._center = center;
  }

  get center(): Array < number > {
    return this._center;
  }

  @Input()
  set basemap(basemap: string) {
    this._basemap = basemap;
  }

  get basemap(): string {
    return this._basemap;
  }

  @Input()
  set maxzoom(maxzoom: number) {
    this._maxzoom = maxzoom;
  }

  get maxzoom(): number {
    return this._maxzoom;
  }

  @Input()
  set minzoom(minzoom: number) {
    this._minzoom = minzoom;
  }

  get minzoom(): number {
    return this._minzoom;
  }

  constructor(public mapService: MapService) {
    mapService.setMap(this);
  }



  async initializeMap() {
    try {
      // Load the modules for the ArcGIS API for JavaScript
      const [
        EsriMap,
        EsriMapView,
        Fullscreen,
        Graphic,
        GraphicsLayer,
        WebMercatorUtils,
        Draw,
        Extent,
        SketchViewModel,
        Polyline,
        Polygon,
        Point,
        Circle,
        geometryEngine,
        Expand,
        Legend,
        Search,
        watchUtils,
        geodesicUtils,
        MapView,
      ] = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/widgets/Fullscreen',
        'esri/Graphic',
        'esri/layers/GraphicsLayer',
        'esri/geometry/support/webMercatorUtils',
        'esri/views/draw/Draw',
        'esri/geometry/Extent',
        "esri/widgets/Sketch/SketchViewModel",
        "esri/geometry/Polyline",
        "esri/geometry/Polygon",
        "esri/geometry/Point",
        "esri/geometry/Circle",
        "esri/geometry/geometryEngine",
        "esri/widgets/Expand",
        "esri/widgets/Legend",
        "esri/widgets/Search",
        "esri/core/watchUtils",
        "esri/geometry/support/geodesicUtils",
      ]);

      // GraphicsLayer to hold graphics created via sketch view model
      let g_layer = new GraphicsLayer();

      // Configure the Map
      const mapProperties: esri.MapProperties = {
        basemap: this._basemap,
        layers: [g_layer]
      };
      const map: esri.Map = new EsriMap(mapProperties);
      // Initialize the MapView
      const mapViewProperties: esri.MapViewProperties = {
        container: this.mapViewEl.nativeElement,
        center: this._center,
        zoom: this._zoom,
        map: map,
        constraints: {
          minZoom: this._minzoom,
          maxZoom: this._maxzoom
        }
      };
      const mapView = new EsriMapView(mapViewProperties);

      // Create a symbol for rendering the graphic
      this.setupSymbols();

      this._mapView = mapView;
      this._fullscreen = new Fullscreen({
        view: mapView
      });
      this._Sketch = new SketchViewModel({
          view: mapView,
          layer: g_layer,
          defaultUpdateOptions: {
            enableRotation: false, // default value
            enableScaling: false,
            toggleToolOnClick: false,
            multipleSelectionEnabled: false
          },
          defaultCreateOptions: {
            mode: "freehand" // "hybrid"|"freehand"|"click"
          },
        activeFillSymbol: this._fillSymbol
      });
      // define arcgis js function to be used later
      this._Graphic_func = Graphic;
      this._Point_func = Point;
      this._webMercatorUtils_func = WebMercatorUtils;
      this._Polyline_func = Polyline;
      this._geometryEngine_func = geometryEngine;
      this._geodesicUtils_func = geodesicUtils;
      this._Extent_func = Extent;
      this._Polygon_func = Polygon;
      this._Circle_func = Circle;

      this._radius_scale = (this.radiusUnit === "meters") ? 1 : (this.radiusUnit === "kilometers") ? 1000 : undefined;
      this.earth_radius_km = (this.radiusUnit === "meters") ? this.earth_radius_km * 1000 : this.earth_radius_km;
      this.min_radius_km = (this.radiusUnit === "meters") ? this.min_radius_km * 1000 : this.min_radius_km;


      this.defaults();
      this.setUpAppUI();
      this.setupSketch();

      return mapView;

    } catch (error) {
      console.log('EsriLoader: ', error);
    }

  }

  initLocationFilter() {
    // round the numbers
    this.round();

    if (!this.is_geopoint) {
      this.clean_borderColor(this.maxlatEl.nativeElement);
      this.clean_borderColor(this.minlatEl.nativeElement);
      this.clean_borderColor(this.maxlonEl.nativeElement);
      this.clean_borderColor(this.minlonEl.nativeElement);
      this.location = {
        bbox: {
          maxlat: this.maxlat,
          minlat: this.minlat,
          maxlon: this.maxlon,
          minlon: this.minlon
        },
        is_geopoint: this.is_geopoint
      };
    } else {
      this.clean_borderColor(this.radiusEl.nativeElement);
      this.clean_borderColor(this.latEl.nativeElement);
      this.clean_borderColor(this.lonEl.nativeElement);
      if (this.Circle_2_Rectangle) {
        this.location = {
          bbox: {
            maxlat: this.maxlat,
            minlat: this.minlat,
            maxlon: this.maxlon,
            minlon: this.minlon
          },
          is_geopoint: !this.Circle_2_Rectangle
        };
      } else {
        this.location = {
          point: {
            lat: this.lat,
            lon: this.lon,
            radius: this.radius,
            radiusUnit: this.radiusUnit
          },
          is_geopoint: this.is_geopoint
        };
      }
    }
    // nottfy other components
    this.mapService.setNewCoords(this.location);
    this._show_coordinates = true;
    this.set_notification_area();
    this.input_changed_Ev = false;
  }

  cleanSketch() {
    if (this._Sketch) {
      this._Sketch.layer.removeAll();
      this._Sketch.view.graphics.removeAll();
      this._Sketch.view.graphics.remove(this._labelGraphic);
    }
  }


  drawCircle(x, y, radius, inLATLon=true) {
    //
    if (inLATLon) {
      [x, y]= this._webMercatorUtils_func.lngLatToXY(x, y);
    }

    // get start point
    this._start_point = new this._Point_func({
      x: x,
      y: y,
      spatialReference: this._Sketch.view.spatialReference
    });

    this._centerGraphic = new this._Graphic_func({
      geometry: this._start_point,
      symbol: this._pointSymbol,
      attributes: {
        center: "center"
      }
    });


    this._end_point = this._geodesicUtils_func.pointFromDistance(
      new this._Point_func({ x: x, y: y}),
      radius * this._radius_scale,
      this._polylineAzimuth
    );

    this._edgeGraphic = new this._Graphic_func({
      geometry: this._end_point,
      symbol: this._pointSymbol,
      attributes: {
        edge: "edge"
      }
    });

    // get vertices
    this._vertices = [
      [x, y],
      this._webMercatorUtils_func.lngLatToXY(this._end_point.x, this._end_point.y)
    ];

    this._polyline = new this._Polyline_func({
      paths: this._vertices,
      spatialReference: this._Sketch.view.spatialReference
    });

    this._polylineGraphic = new this._Graphic_func({
      geometry: this._polyline,
      symbol: this._polylineSymbol
    });

    this._labelGraphic = this.label_geometry(this._end_point, this.radius, this.radiusUnit);

      // create buffer
      // const gra = this._geometryEngine_func.geodesicBuffer(this._start_point, this.radius, this.radiusUnit);
      // or create circle
      const gra = new this._Circle_func({
        center: this._start_point,
        geodesic: true,
        numberOfPoints: 500,
        radius: this.radius,
        radiusUnit: this.radiusUnit
      });
    if (this.Circle_2_Rectangle) {
      [this.maxlon, this.maxlat] = this._webMercatorUtils_func.xyToLngLat(gra.extent.xmax, gra.extent.ymax);
      [this.minlon, this.minlat] = this._webMercatorUtils_func.xyToLngLat(gra.extent.xmin, gra.extent.ymin);
      // clean skitch
      this.cleanSketch();
      this.drawRectangle(this.minlon, this.maxlon, this.minlat, this.maxlat, true);
      this.round();
      // clean skitch
      this.cleanSketch();
      this.drawRectangle(this.minlon, this.maxlon, this.minlat, this.maxlat, true);
    } else {
      // Add buffer to layer used with sketchVM
      const bufferGraphic = new this._Graphic_func({
        geometry: gra,
        symbol: this._fillSymbol
      });
      this._Sketch.layer.graphics.add(bufferGraphic)
    }
    // Add graphics to view used with sketchVM
    this._Sketch.view.graphics.addMany([
      this._centerGraphic
    ]);
  }


  drawRectangle(xmin, xmax, ymin, ymax, inLATLon = true){
    if (inLATLon) {
      const max_point = this._webMercatorUtils_func.lngLatToXY(xmax, ymax);
      const min_point = this._webMercatorUtils_func.lngLatToXY(xmin, ymin);
      xmax = max_point[0];
      ymax = max_point[1];
      xmin = min_point[0];
      ymin = min_point[1];
    }
    // create extent geometry
    // const geom = new this._Extent_func({
    //   xmin: xmin,
    //   xmax: xmax,
    //   ymin: ymin,
    //   ymax: ymax,
    //   spatialReference: this._Sketch.view.spatialReference
    // });
    // create Polygon geometry
    const geom = new this._Polygon_func({
      rings: [[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]],
      spatialReference: this._Sketch.view.spatialReference
    });

    const newGraphic = new this._Graphic_func({
      geometry: geom,
      symbol: this._fillSymbol
    });
    this._Sketch.layer.graphics.add(newGraphic);
  }

  setUpAppUI() {

    // add ui buttons
    this._mapView.ui.add(this._fullscreen, 'top-right');
    this._mapView.ui.add('delete-button', 'top-right');
    this._mapView.ui.add('rect-button', 'top-right');
    if (this.activate_Circle) {
      this._mapView.ui.add('circle-button', 'top-right');
    }
    this._mapView.ui.add('apply-button', 'top-right');

    // set notification area
    this._mapView.on("pointer-move", evt => {
      if (this._drawing) {
        this.set_notification_area(1);
      }
    });
    this._mapView.on("pointer-leave", evt => {
      if (this._drawing) {
        this.set_notification_area(2);
      }
    });

  }

  setupSketch(){
    if (this._Sketch) {

      // Listen to sketchViewModel's create event.
      this._Sketch.on("create", e => {
        if (
          e.toolEventInfo &&
          e.toolEventInfo.type === "vertex-add"
        ) {
          if (e.state === "start") {
            this._vertices = [e.toolEventInfo.vertices[0].coordinates];
            this._start_point = new this._Point_func({
              x: this._vertices[0][0],
              y: this._vertices[0][1],
              spatialReference: this._Sketch.view.spatialReference
            });
            [this._lon_start, this._lat_start] = this._webMercatorUtils_func.xyToLngLat(
              this._start_point.x,
              this._start_point.y
            );
          }
          if (e.state === "active") {
            this._vertices.push(e.toolEventInfo.vertices[0].coordinates);
            this._end_point = new this._Point_func({
              x: this._vertices[1][0],
              y: this._vertices[1][1],
              spatialReference: this._Sketch.view.spatialReference
            });
          }
        }
        if (e.state === "complete") {
          [this._lon_end, this._lat_end] = this._webMercatorUtils_func.xyToLngLat(
            this._end_point.x,
            this._end_point.y
          );
          // Create the graphics representing the line and buffer
          if (this.is_geopoint) {

            this._polyline = new this._Polyline_func({
              paths: this._vertices,
              spatialReference: this._Sketch.view.spatialReference
            });

            // get the length of the initial polyline and create buffer
            this.radius = this._geometryEngine_func.geodesicLength(this._polyline, this.radiusUnit);
            // get lat lon
            this.lat = this._lat_start;
            this.lon = this._lon_start;
            // round
            this.round();

            // this.cleanSketch();
            // this.drawCircle(this.lon, this.lat, this.radius);
            this._centerGraphic = new this._Graphic_func({
              geometry: this._start_point,
              symbol: this._pointSymbol,
              attributes: {
                center: "center"
              }
            });

            this._end_point = this._geodesicUtils_func.pointFromDistance(
              new this._Point_func({ x: this.lon, y: this.lat }),
              this.radius * this._radius_scale,
              this._polylineAzimuth
            );

            this._edgeGraphic = new this._Graphic_func({
              geometry: this._end_point,
              symbol: this._pointSymbol,
              attributes: {
                edge: "edge"
              }
            });

            this._vertices = [
              [this._start_point.x, this._start_point.y],
              this._webMercatorUtils_func.lngLatToXY(this._end_point.x, this._end_point.y)
            ];

            this._polyline = new this._Polyline_func({
              paths: this._vertices,
              spatialReference: this._Sketch.view.spatialReference
            });

            this._polylineGraphic = new this._Graphic_func({
              geometry: this._polyline,
              symbol: this._polylineSymbol
            });

            this._labelGraphic = this.label_geometry(this._end_point, this.radius, this.radiusUnit);
            if (this.Circle_2_Rectangle) {
              [this.maxlon, this.maxlat] = this._webMercatorUtils_func.xyToLngLat(e.graphic.geometry.extent.xmax, e.graphic.geometry.extent.ymax);
              [this.minlon, this.minlat] = this._webMercatorUtils_func.xyToLngLat(e.graphic.geometry.extent.xmin, e.graphic.geometry.extent.ymin);
              // clean skitch
              this.cleanSketch();
              this.drawRectangle(this.minlon, this.maxlon, this.minlat, this.maxlat, true);
            }

            // Add graphics to layer used with sketchVM
            this._Sketch.view.graphics.addMany([
              this._centerGraphic
            ]);

          } else {
            this.maxlat = Math.max(this._lat_start, this._lat_end);
            this.minlat = Math.min(this._lat_start, this._lat_end);
            this.minlon = Math.min(this._lon_start, this._lon_end);
            this.maxlon = Math.max(this._lon_start, this._lon_end);
            // round
            this.round();
          }
          // change location filter
          this.initLocationFilter();
        }
      });

      // Listen to SketchViewModel's update event so that population pyramid chart
      // is updated as the graphics are updated
      this._Sketch.on("update", e => {

        if (e.toolEventInfo) {
          let gg = this._Sketch.view.graphics.filter(function (g) {
            return (
              g.hasOwnProperty("attributes") &&
              (
                (
                  g.attributes.hasOwnProperty("center") &&
                  g.attributes.center === "center"
                )
                ||
                (
                  g.attributes.hasOwnProperty("edge") &&
                  g.attributes.edge === "edge"
                )
              )
            );
          });
          gg.push(this._polylineGraphic, this._labelGraphic);
          this._Sketch.view.graphics.removeMany(gg);
        }
        if (
          e.toolEventInfo &&
          e.toolEventInfo.type.includes("-stop")
        ) {
          if (this.is_geopoint) {
            this._start_point = e.toolEventInfo.graphics[0].geometry.centroid;
            this._centerGraphic = new this._Graphic_func({
              geometry: this._start_point,
              symbol: this._pointSymbol,
              attributes: {
                center: "center"
              }
            });
            [this.lon, this.lat] = this._webMercatorUtils_func.xyToLngLat(
              this._start_point.x,
              this._start_point.y
            );

            this.round();

            this._end_point = this._geodesicUtils_func.pointFromDistance(
              new this._Point_func({ x: this.lon, y: this.lat }),
              this.radius * this._radius_scale,
              this._polylineAzimuth
            );

            this._edgeGraphic = new this._Graphic_func({
              geometry: this._end_point,
              symbol: this._pointSymbol,
              attributes: {
                edge: "edge"
              }
            });

            const vertices = [
              [this._start_point.x, this._start_point.y],
              this._webMercatorUtils_func.lngLatToXY(this._end_point.x, this._end_point.y)
            ];

            const polyline = new this._Polyline_func({
              paths: vertices,
              spatialReference: this._Sketch.view.spatialReference
            });

            this._polylineGraphic = new this._Graphic_func({
              geometry: polyline,
              symbol: this._polylineSymbol
            });

            this._labelGraphic = this.label_geometry(this._end_point, this.radius, this.radiusUnit);

            // graphicsLayer.addMany([centerGraphic]);
            this._Sketch.view.graphics.addMany([
              this._centerGraphic
            ]);

            if (this.Circle_2_Rectangle) {
              const gra = new this._Circle_func({
                center: this._start_point,
                geodesic: true,
                numberOfPoints: 500,
                radius: this.radius,
                radiusUnit: this.radiusUnit
              });
              [this.maxlon, this.maxlat] = this._webMercatorUtils_func.xyToLngLat(gra.extent.xmax, gra.extent.ymax);
              [this.minlon, this.minlat] = this._webMercatorUtils_func.xyToLngLat(gra.extent.xmin, gra.extent.ymin);
            }

          } else {
            const corners = e.toolEventInfo.graphics[0].geometry.rings[0];
            const lat_mat = [];
            const lon_mat = [];
            corners.forEach(([x, y]) => {
              [this._lon_start, this._lat_start] = this._webMercatorUtils_func.xyToLngLat(x, y);
              if (!lat_mat.includes(this._lat_start)) {
                lat_mat.push(this._lat_start);
              }
              if (!lon_mat.includes(this._lon_start)) {
                lon_mat.push(this._lon_start);
              }
            });
            this.minlat = Math.min(...lat_mat);
            this.maxlat = Math.max(...lat_mat);
            this.minlon = Math.min(...lon_mat);
            this.maxlon = Math.max(...lon_mat);
            this.round();
          }
          // change location filter
          this.initLocationFilter();
        }
      });
    }
  }

  setupSymbols() {
    this._fillSymbol = {
      type: 'simple-fill', // autocasts as new Simple_fillSymbol()
      color: [227, 139, 79, 0.8],
      outline: { // autocasts as new SimpleLineSymbol()
        color: [50, 50, 50],
        width: 0
      }
    };

    this._pointSymbol = {
      type: "simple-marker",
      style: "circle",
      size: 6,
      color: [116, 153, 52, 0.8],
      outline: {
        color: [50, 50, 50],
        width: 0
      }
    };

    this._polylineSymbol = {
      type: "simple-line",
      color: [254, 254, 254, 1],
      width: 1.5
    };
  }

  defaults(){
    this._show_coordinates = false;
    this.maxlat = undefined;
    this.maxlon = undefined;
    this.minlat = undefined;
    this.minlon = undefined;
    this.lat = undefined;
    this.lon = undefined;
    this.radius = undefined;
    this.input_changed_Ev = false;
    this.container_temp = undefined;
    // this.is_geopoint = true;
  }

  // Finalize a few things once the MapView has been loaded
  houseKeeping(mapView) {
    mapView.when(() => {
      this._loaded = mapView.ready;
      this.mapLoadedEvent.emit(true);
    });
  }

  select_circle() {
    this.is_geopoint = true;
    this._drawing = true;
    this.defaults();
    this.mapService.setDefaultCoords();
    this._Sketch.defaultUpdateOptions.enableRotation = false;
    this._Sketch.defaultUpdateOptions.enableScaling = false;
    // this._Sketch.defaultUpdateOptions.enableScaling = this.Circle_2_Rectangle;
    this._Sketch.defaultUpdateOptions.multipleSelectionEnabled = false;
    this._Sketch.defaultUpdateOptions.toggleToolOnClick = false;
    this.cleanSketch();
    this._Sketch.create("circle", { mode: "freehand" });
    // this._Sketch.create("rectangle", { mode: "freehand" });
    this.set_notification_area(1);
  }

  // return a new graphic to be added to the map
  label_geometry(geom, length, length_unit) {
    return new this._Graphic_func({
      geometry: geom,
      symbol: {
        type: "text",
        color: "#6e4209",
        text: length.toFixed(2) + " " + length_unit,
        xoffset: 50,
        yoffset: 10,
        font: {
          // autocast as Font
          size: 12,
          family: "sans-serif"
        }
      }
    });
  }

  select_rect() {
    this.is_geopoint = false;
    this._drawing = true;
    this.defaults();
    this.mapService.setDefaultCoords();
    this._Sketch.defaultUpdateOptions.enableRotation = false;
    this._Sketch.defaultUpdateOptions.enableScaling = true;
    this._Sketch.defaultUpdateOptions.multipleSelectionEnabled = false;
    this._Sketch.defaultUpdateOptions.toggleToolOnClick = false;
    this.cleanSketch();
    this._Sketch.create("rectangle", { mode: "freehand" });
    this.set_notification_area(1);
  }

  clean() {
    this._mapView.when(() => {
      this._mapView.graphics.removeAll();
    });
    this.set_notification_area();
    this._drawing = false;
    this.mapService.setDefaultCoords();
    this.defaults();
    this.cleanSketch();
  }

  draw(type) {
    this.is_geopoint = (type==='c') ? true : false
    if (this.is_geopoint) {
      this.select_circle();
    } else {
      this.select_rect();
    }
  }

  set_notification_area(msg=0) {
    switch (msg) {
      case 1:
        this.notification_area = "Please click on the map and drag the mouse to select your area.";
        break;
      case 2:
        this.notification_area = "Please go back to the map then click on it and drag the mouse to select your area.";
        break;
      default:
        this.notification_area = "";
        break;
    }
  }

  roundpoint(fix_Nr){
    if (this.radius) {
      this.radius = this.limitNumberWithinRange(Number(Number(this.radius).toFixed(fix_Nr)), this.min_radius_km, this.earth_radius_km);
    }

    if (this.lat) {
      this.lat = this.limitNumberWithinRange(Number(Number(this.lat).toFixed(fix_Nr)), -90, 90);
    }

    if (this.lon) {
      this.lon = this.limitNumberWithinRange(Number(Number(this.lon).toFixed(fix_Nr)), -180, 180);
    }
  }
  round_rect(fix_Nr){
    if (this.maxlat) {
      this.maxlat = this.limitNumberWithinRange(Number(Number(this.maxlat).toFixed(fix_Nr)), -90, 90);
    }

    if (this.minlat) {
      this.minlat = this.limitNumberWithinRange(Number(Number(this.minlat).toFixed(fix_Nr)), -90, 90);
    }

    if (this.minlon) {
      this.minlon = this.limitNumberWithinRange(Number(Number(this.minlon).toFixed(fix_Nr)), -180, 180);
    }

    if (this.maxlon) {
      this.maxlon = this.limitNumberWithinRange(Number(Number(this.maxlon).toFixed(fix_Nr)), -180, 180);
    }
  }

  round(fix=3) {
    if (this.is_geopoint) {
      this.roundpoint(fix);
      if (this.Circle_2_Rectangle) {
        this.round_rect(fix);
      }
    } else {
      this.round_rect(fix);
    }
  }

  apply(){
    // get the new changes
    if (this.container_temp) {
      for (const key of Object.keys(this.container_temp).filter(k => !k.endsWith("_temp") && !k.endsWith("_ev"))) {
        this[key] = this.container_temp[`${key}_temp`];
        this.container_temp[key] = this[key];
      }
    }
    // round
    this.round();
    // clean skitch
    this.cleanSketch();
    // add to skich layer
    if (this.is_geopoint) {
      this.drawCircle(this.lon, this.lat, this.radius, true);
    } else {
      // reorder lon,lat
      const maxlat = this.maxlat;
      const minlat = this.minlat;
      const maxlon = this.maxlon;
      const minlon = this.minlon;
      this.maxlat = (maxlat < minlat) ? minlat : maxlat;
      this.minlat = (minlat > maxlat) ? maxlat : minlat;
      this.maxlon = (maxlon < minlon) ? minlon : maxlon;
      this.minlon = (minlon > maxlon) ? maxlon : minlon;
      this.drawRectangle(this.minlon, this.maxlon, this.minlat, this.maxlat, true);
    }
    // change location filter
    this.initLocationFilter();
  }

  limitNumberWithinRange(num, min = -99999, max = 99999) {
    const parsed = parseFloat(num);
    return Math.min(Math.max(parsed, min), max)
  }

  initiate_keyupEventFun(el, evType) {
    fromEvent(el, evType)
      .pipe(
        // Time in milliseconds between key events
        debounceTime(50)
        )
      .subscribe((e: any) => {
        // convert to number
        const val = Number(e.target.value);
        if (!Number.isNaN(Number(val))) {
          this.clean_borderColor(e.target, false);
          if (!this.container_temp) {
            this.container_temp = {};
          }
          if (!this.container_temp.hasOwnProperty(e.target.id)) {
            this.container_temp[e.target.id] = this[e.target.id];
          }
          this.container_temp[`${e.target.id}_temp`] = val;
          this.container_temp[`${e.target.id}_ev`] = this.container_temp[e.target.id] != this.container_temp[`${e.target.id}_temp`];
          this.input_changed_Ev = Object.entries(this.container_temp)
            .filter(([k, v]) => k.endsWith("_ev"))
            .map(([k, v]) => { return v })
            .some(b => b);

          // move enetr to hier in case enter was pressed too fast then will be applies after timeout
          if (e.key == 'Enter' && this.input_changed_Ev) {
            this.apply();
          }
        } else {
          e.target.style["border-color"] = "red";
        }
      });
  }

  clean_borderColor(el, reset_val=true) {
    el.style["border-top-color"] = "";
    el.style["border-right-color"] = "";
    el.style["border-bottom-color"] = "";
    el.style["border-left-color"] = "";
    if (reset_val) {
      el.value = this[el.id];
    }
  }

  initiate_keyupEvents(ev){
    this.initiate_keyupEventFun(this.maxlatEl.nativeElement, ev);
    this.initiate_keyupEventFun(this.minlatEl.nativeElement, ev);
    this.initiate_keyupEventFun(this.maxlonEl.nativeElement, ev);
    this.initiate_keyupEventFun(this.minlonEl.nativeElement, ev);
    this.initiate_keyupEventFun(this.latEl.nativeElement, ev);
    this.initiate_keyupEventFun(this.lonEl.nativeElement, ev);
    this.initiate_keyupEventFun(this.radiusEl.nativeElement, ev);
  }

  ngAfterViewInit() {
    this.initiate_keyupEvents('keyup');
  }

  ngOnInit() {
    // Initialize MapView and return an instance of MapView
    this.initializeMap().then((mapView) => {
      this.houseKeeping(mapView);
    });
  }

}
