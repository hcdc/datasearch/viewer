import { Component, OnInit, Input } from '@angular/core';
import { Collapsible } from '../services/collapsible';

@Component({
  selector: 'app-collapsecontroller',
  templateUrl: './collapsecontroller.component.html',
  styleUrls: ['./collapsecontroller.component.css']
})
/**
 * creates a button to open/close an element that is
 * rendered in a collapsecontainer
 * this control and that element communicate via the service
 * that is passed through the input
 */
export class CollapsecontrollerComponent implements OnInit {
  @Input() service: Collapsible;
  @Input() classes: string;
  @Input() tooltiptext: string;
  @Input() activeText: string = " (active)";
  constructor() { }

  ngOnInit(): void {
  }

}
