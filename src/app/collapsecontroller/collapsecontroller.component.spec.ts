import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CollapsecontrollerComponent } from './collapsecontroller.component';

describe('CollapsecontrollerComponent', () => {
  let component: CollapsecontrollerComponent;
  let fixture: ComponentFixture<CollapsecontrollerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CollapsecontrollerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsecontrollerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
