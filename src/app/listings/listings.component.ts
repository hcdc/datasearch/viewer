import { Subscription } from 'rxjs/internal/Subscription';
import { UtilService } from './../services/util.service';
import { FilterService } from './../services/filter.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ResultService } from '../services/result.service';
import * as Constants from '../constants';

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.css']
})
export class ListingsComponent implements OnInit, OnDestroy {
  @Input() hit: any;
  listings = [];
  listingsSubscription: Subscription;
  close_Others = false;
  constructor(
    public filterService: FilterService,
    public resultService: ResultService,
    public utilService: UtilService
    ) { }
  ngOnDestroy(): void {
    // this.listingsSubscription.unsubscribe();
  }

    ngOnInit(): void {
      this.getListings();
    }
  getListings() {
    // const exploded = this.explodeValues(this.hit.payload);
    // const filter = this.filterService.getFilter();
    this.listings = this.hit.listed_values;

    this.listings.forEach( dict =>
      dict.val.class = Constants.TYPETOCSSCLASS[dict.key] || dict.key
    )
    // Object.keys(this.listings).forEach(key=>{
    //   this.listings[key].class = Constants.TYPETOCSSCLASS[key] || key
    // })

    // this.listingsSubscription = this.resultService.listValues(exploded, filter, this.hit.meta.Source).subscribe(result => {
    //   if (result) {
    //     this.listings = result.hits;
    //   }
    // });
  }

  create_list_mappings(index_name, type) {
    return { [index_name]: [type] }
  }
  explodeValues(field) {
    const result = [];
    Object.keys(field).forEach(type => {
        const fieldAttr = field[type];
        if (Array.isArray(fieldAttr)) {
            fieldAttr.forEach(input => {
              result.push({input, type});
            });
        } else {
          result.push({input: fieldAttr, type});
        }
    });
    return result;
  }
}
