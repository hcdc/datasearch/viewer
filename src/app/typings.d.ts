import { ResultitemComponent } from './resultitem/resultitem.component';


interface Coords {
  minlat: number;
  minlon: number;
  maxlat: number;
  maxlon: number;
}
interface DateRange {
  start: Date;
  end: Date;
}
interface FilterItem {
  input: string;
  type: string;
  class: string
}
interface Suggestion {
  type: string;
  input: string;
}
interface Cart {
  id: number;
  email: string;
  items: CartItem[];
}
interface MetaData {
  source: string;
  enddate: Date;
  startdate: Date;
  coords: Coords;
}
interface CartItem {
  id: number;
  hit: Hit;
}

interface Hit {
  payload: FilterItem[];
  meta: MetaData;
  problems: string;
  excludedFields: FilterItem[];
  downloadFormats: string[];
  chosenFormats: string[];
  inCart: boolean;
}
interface CollapseState {
  name: string;
  iscollapsed: boolean;
}

interface Dictionary<T> {
  [Key: string]: T;
}
interface location {
  bbox?: Dictionary<number>;
  point?: Dictionary<number>;
  is_geopoint: boolean;
}
interface TimeRange {
  start: string;
  end: string;
}
interface Suggest {
  prefix: string;
  time: TimeRange;
  location: location;
  filter?: FilterItem[];
}
