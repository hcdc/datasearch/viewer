import { ResultService } from './services/result.service';
import 'core-js';
import { MapService } from './services/map.service';
import { FilterService } from './services/filter.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { EsriMapComponent } from './esri-map/esri-map.component';
import { SearchComponent } from './search/search.component';
import { FilterComponent } from './filter/filter.component';
import { ResultComponent } from './result/result.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResultitemComponent } from './resultitem/resultitem.component';
import { MapWrapperComponent } from './mapwrapper/mapwrapper.component';
import { TimerangeselectComponent } from './timerangeselect/timerangeselect.component';
import { CollapsecontrollerComponent } from './collapsecontroller/collapsecontroller.component';
import { CollapsecontainerComponent } from './collapsecontainer/collapsecontainer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartComponent } from './cart/cart.component';
import { UtilService } from './services/util.service';
import { ListingsComponent } from './listings/listings.component';
import { CollapseService } from './services/collapse.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { SuggestService } from './services/suggest.service';
import { FooterComponent } from './footer/footer.component';
import { APIComponent } from './api/api.component';
@NgModule({
  declarations: [
    AppComponent,
    EsriMapComponent,
    SearchComponent,
    FilterComponent,
    ResultComponent,
    DatepickerComponent,
    ResultitemComponent,
    MapWrapperComponent,
    TimerangeselectComponent,
    CollapsecontrollerComponent,
    CollapsecontainerComponent,
    CartComponent,
    ListingsComponent,
    FooterComponent,
    APIComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgSelectModule,
  ],
  bootstrap: [
    AppComponent,
    APIComponent
  ],
  providers: [
    MapService,
    FilterService,
    FilterComponent,
    UtilService,
    CollapseService,
    ResultService,
    SuggestService
  ]
})
export class AppModule {}
