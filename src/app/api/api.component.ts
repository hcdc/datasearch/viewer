import { Component, OnInit } from '@angular/core';

import { environment } from '../../environments/environment';




@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class APIComponent implements OnInit {

  public readonly API_URL = environment.API_URL;

  public readonly api_title = "Helmholtz-Zentrum Hereon datasearch portal API";

  constructor() { }

  ngOnInit(): void {
  }

}
