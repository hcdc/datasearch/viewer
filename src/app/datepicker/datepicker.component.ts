import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import { filter } from 'rxjs/internal/operators/filter';
import { map } from 'rxjs/internal/operators/map';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { fromEvent } from 'rxjs/internal/observable/fromEvent';
import {NgbDateStruct, NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { DateService } from '../services/date.service';
@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit, OnDestroy {
  @Input() isStartDate: boolean;
  currentDate:Date;
  model: NgbDateStruct;
  minDate: NgbDateStruct = this.jsDateToNgbDate(this.dateService.getMinDate());
  maxDate: NgbDateStruct = this.jsDateToNgbDate(this.dateService.getMaxDate());
  @ViewChild('datepicker')
  dateinput: ElementRef;
  keyupSubscription: Subscription;
  keyupEnterSubscription: Subscription;
  dateSubscription: Subscription;
  constructor(public calendar: NgbCalendar,
              public formatter: NgbDateParserFormatter,
              public dateService: DateService) {
  }
  ngAfterViewInit(): void {
    // not Enter key
    this.keyupSubscription = fromEvent(this.dateinput.nativeElement, 'keyup').pipe(
      filter((event: any) => event.key !== 'Enter'),
      map((event: any) => {
        return event.target.value;
      })
      // Time in milliseconds between key events
      , debounceTime(800)
    ).subscribe({ next: (prefix: string) => this.dateValidation(prefix) });
        // Enter key
    this.keyupEnterSubscription = fromEvent(this.dateinput.nativeElement, 'keyup').pipe(
          filter((event: any) => event.key === 'Enter'),
          map((event: any) => {
            return event.target.value;
          })
    ).subscribe({ next: (prefix: string) => this.setDate(prefix) });
  }
  dateValidation(prefix){
    //const v = this.dateService.isvalidDateFormat(prefix.replace(/-0?0?/g, '-'));
    //const vr = this.dateService.dateInRange(prefix.replace(/-0?0?/g, '-'));
    const v = this.dateService.isvalidDateFormat(prefix);
    const vr = this.dateService.dateInRange(prefix);
    if (this.isStartDate) {
      this.dateService.isvaildStartDate = (!v) ? 1 : ((!vr) ? 4 : 0);
    }else{
      this.dateService.isvaildEndDate = (!v) ? 2 : ((!vr) ? 5 : 0);
    }
    this.dateService.errormessagekey = (this.dateService.isvaildStartDate === 0 && this.dateService.isvaildEndDate === 0) ? 0 :
    ((this.dateService.isvaildStartDate > 0) ? this.dateService.isvaildStartDate : this.dateService.isvaildEndDate);
    clearTimeout(this.dateService.timeoutSubject);
    if (this.dateService.errormessagekey === 0) {
      this.setTimeRange(this.dateService.getdatefromstr(prefix));
    }else{
      this.resetCalendarTimer();
    }


  }
  setDate(prefix){
    if (prefix && this.dateService.errormessagekey === 0) {
      this.setTimeRange(this.dateService.getdatefromstr(prefix));
    }
  }
  onNavigate(event) {
    const targetMonth = event.next.month;
    const targetYear = event.next.year;
    let fError  = (this.model) ?  false: true;
    if (!fError && this.dateService.errormessagekey === 0) {
        const selectedDay = (!this.model.day) ? 1 : this.model.day;
        const d = {
          year: targetYear,
          month: targetMonth,
          day: selectedDay
        };
        fError = (this.ngbDateToJsDate(d) <= this.ngbDateToJsDate(this.calendar.getToday())) ? false : true;
        if (!fError) {
          this.model = d;
          if (this.isStartDate) {
            this.dateService.setNewStartDate(this.ngbDateToJsDate(this.model));
          } else {
            this.dateService.setNewEndDate(this.ngbDateToJsDate(this.model));
          }
        }
      }
      if (fError) {
        this.dateService.errormessagekey = (this.isStartDate) ? 4 : 5;
        this.resetCalendarTimer();
      }
  }
  ngOnInit(): void {
    // ngb datpicker only detects change on reference not on value
    // whole model must be set on change
    if(this.isStartDate){
      this.dateSubscription = this.dateService.startDate$.subscribe(sd => {
        this.maxDate = this.jsDateToNgbDate(this.dateService.timeRangeBS$.value.end);
        this.model = this.jsDateToNgbDate(sd);
      });
    } else {
      this.dateSubscription = this.dateService.endDate$.subscribe(ed => {
        this.minDate = this.jsDateToNgbDate(this.dateService.timeRangeBS$.value.start);
        this.model = this.jsDateToNgbDate(ed);
      });
    }
  }
  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
    this.keyupEnterSubscription.unsubscribe();
    this.keyupSubscription.unsubscribe();
  }

  setTimeRange(ngbDate) {
    let err = false;
    ngbDate = this.ngbDateToJsDate(ngbDate);
    if (this.isStartDate) {
      if (ngbDate > this.dateService.timeRangeBS$.value.end) err = true;
      this.dateService.setNewStartDate(ngbDate);
    } else {
      if (ngbDate < this.dateService.timeRangeBS$.value.start) err = true;
      this.dateService.setNewEndDate(ngbDate);
    }
    if (err) this.resetCalendarTimer();
  }
  jsDateToNgbDate(jsDate) {
    return { day: jsDate.getDate(), month: jsDate.getMonth() +1, year: jsDate.getFullYear() }
  }
  ngbDateToJsDate(ngbDate){
    return new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
  }
  resetCalendarTimer() {
    clearTimeout(this.dateService.timeoutSubject);
    this.dateService.timer();
    this.dateService.timeoutSubject = setTimeout(() => { this.resetCalendar(); }, this.dateService.waitingResetTimeout);
  }
  resetCalendar() {
    this.dateService.reset(this.isStartDate);
  }
  today(){
    let err , ngbDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    if (this.isStartDate && ngbDate > this.dateService.timeRangeBS$.value.end) err = true;
    if (!this.isStartDate && ngbDate < this.dateService.timeRangeBS$.value.start) err = true;
    if (!this.model) err = true;
    //this.model = this.calendar.getToday()
    this.dateService.today(this.isStartDate);
    if (err) this.resetCalendarTimer();
  }
}
