import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { UtilService } from '../services/util.service';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
/**
 * the shopping cart
 */
export class CartComponent implements OnInit {
  FormData: FormGroup;
  default='';
  constructor(
    public cartService: CartService,
    public utilService: UtilService,
    private builder: FormBuilder
  ) { }


  ngOnInit(): void {
    /**
     * validates the emailaddress
     */
    this.FormData = this.builder.group({
      email: new FormControl('', [Validators.compose([Validators.required, Validators.email])])
    });
  }
}
