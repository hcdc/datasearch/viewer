
import { environment } from '../../environments/environment';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { timeout, catchError } from 'rxjs/operators';
import { of, TimeoutError } from 'rxjs';
import { Injectable } from '@angular/core';
import * as Constants from '../constants';
@Injectable({
    providedIn: 'root'
})
export class UtilService {

    constructor(private http:HttpClient){}

    getSingularNameForType(val: string) {
        return Constants.SINGULAR_NAMES[val] || val;
    }
    getPluralNameForType(val: string) {
        return Constants.PLURAL_NAMES[val] || val;
    }
    addCSSClassToSuggestions(suggestions: {type: string; }[]) {
        const suggs = [];
        if(Array.isArray(suggestions)){
            suggestions.forEach(sugg =>  suggs.push(Object.assign(sugg, { class: Constants.TYPETOCSSCLASS[sugg.type] || sugg.type})));
        }
        return suggs;
    }
    sendPostRequest(endpoint, body) {
        const url = environment.apiUrl + '/' + endpoint;
        return this.http.post(url, body).pipe(
            catchError((err: HttpErrorResponse) => {
                if (err.error instanceof Error) {
                    // A client-side or network error occurred. Handle it accordingly.
                    console.error(err.error);
                } else {
                    // The backend returned an unsuccessful response code.
                    // The response body may contain clues as to what went wrong,
                    return of(err.error);
                }
            }));
    }
    sendTimeoutPostRequest(endpoint, body, due: number, onTimeout: (err: TimeoutError) => void) {
        const url = environment.apiUrl + '/' + endpoint;
        return this.http.post(url, body).pipe(
            timeout(due),
            catchError((err: HttpErrorResponse) => {
                if (err instanceof TimeoutError) {
                    onTimeout(err)
                } else if (err.error instanceof Error) {
                    // A client-side or network error occurred. Handle it accordingly.
                    console.error(err.error);
                } else {
                    // The backend returned an unsuccessful response code.
                    // The response body may contain clues as to what went wrong,
                    return of(err.error);
                }
            }));
    }
    objToArrayOfAttr(obj) {
        const resultArray = [];
        Object.keys(obj).forEach(type => {
            const input = Array.isArray(obj[type]) ? obj[type].join(' and ') : obj[type];
            resultArray.push({type, class: Constants.TYPETOCSSCLASS[type] || '', input});
        });

        return resultArray;
    }
    isTypeValid(value, types){
        let isValid = false;
        types.forEach(type => {
            if(typeof value === type){
                isValid = true;
            }
        });
        return isValid;
    }
    getHash(obj:any):number{
        const datastring = JSON.stringify(obj);
        return this.hash(datastring)
    }
    hash(str:string):number {
        return str.split('').reduce((prevHash, currVal) =>
            // tslint:disable-next-line: no-bitwise
            (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
    }
    objKeysToUpper(obj) {
        let key = '';
        const keys = Object.keys(obj);
        const newobj = {}
        let n = keys.length;
        while (n--) {
            key = keys[n];
            newobj[key.toUpperCase()] = obj[key];
        }
        return newobj;
    }
    isEmptyObj(obj: any):boolean {
        for(const prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
        return true;
      }
    round(num: number, n: number=4): number {
        //return num.toFixed(n)
        return Math.round(num * Math.pow(10, n)) / Math.pow(10, n)
    }
    removeTags(str) {
        if ((str === null) || (str === ''))
            return false;
        else
            str = str.toString();
        // Regular expression to identify HTML tags in
        // the input string. Replacing the identified
        // HTML tag with a null string.
        return str.replace(/(<([^>]+)>)/ig, '');
        //return str.replace(/<\/?[^>]+>/ig,'')
    }
}

