import { Injectable } from '@angular/core';
import { Collapsible } from './collapsible';

@Injectable({
    providedIn: 'root'
})
/**
 * This service tracks and manage the states
 * of the collapsables (other services the extend the abstract collapsable class)
 * Each collapseable calls "track state" in it's constructor" providing a reference
 * to this services which is then used to track it's state
 */
export class CollapseService {
    public collapsables = [];
    public alwaysOpen = ['result'];
    public trackState(c:Collapsible){
        if(!this.getCollapsableByName(c.name)){
            this.collapsables.push(c);
        }
    }
    /**
     * Is called whenever a collapsable is to be opend, to close whatever other collapsables
     * are open.
     * if a collapseable is in the "always open" array, it is never closed
     * @param the collapsable that is is about to be opend
     */
    closeOthers(name: string) {
        this.getOthers(name).forEach(o=>{
            if(!this.alwaysOpen.includes(o.name)){
                o.close()
            }
        });
    }
    getCollapsableByName(name): Collapsible {
        const cs = this.collapsables.filter(c => c.name === name);
        if(cs.length === 1){
            return cs[0];
        }
        return undefined;
    }
    getOthers(name): Collapsible[] {
        return this.collapsables.filter(c => c.name !== name)
    }
    setIsActiveCaption(){

    }
}