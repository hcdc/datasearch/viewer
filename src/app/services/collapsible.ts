import { Injectable, Injector } from '@angular/core';
import { CollapseService } from './collapse.service';

@Injectable()
/**
 * provide a minimize/maximize functionality for
 * services that extend this class
 * If a service extends this class
 * the component that belongs to that Services
 * must be rendered in a collapseContainer.component
 *
 * The state of the service is then tracked in the collapse service
 * So that it is automatically closed whenever another collapseable is opened
 * Additionally it can be toggled by a control
 *
 */
  export abstract class Collapsible {
    public name;
    public collapseService: CollapseService;
    public isCollapsed = true;
    public activeControllCaption = ' !';
    abstract isActive(): boolean;
    toggle(){
      if(this.isCollapsed){
        this.open();
      } else {
        this.close();
      }
    };
    open() {
      this.isCollapsed = false;
      this.collapseService.closeOthers(this.name);
    }
    close(){
      this.isCollapsed = true;
    }
    constructor(private injectorObj: Injector, name){
      this.name = name;
      this.collapseService = this.injectorObj.get(CollapseService);
      this.collapseService.trackState(this);
    }
  }