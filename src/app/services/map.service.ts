import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs';
import { Collapsible } from './collapsible';
import { Coords, location } from '../typings';
import { UtilService } from './util.service';


@Injectable()
export class MapService extends Collapsible{
    map: any;
    private maxExtent = {
        bbox: {
            maxlat: 90,
            minlat: -90,
            maxlon: 180,
            minlon: -180
        },
        is_geopoint: false
    }

    coordsBS$: BehaviorSubject<location> = new BehaviorSubject(
        this.maxExtent
    );
    public readonly coords$: Observable<location> = this.coordsBS$.asObservable();

    constructor(private injector: Injector, private utilService: UtilService,) { super(injector, 'map'); }
    getLatestCoords() {
        return this.coordsBS$.value;
    }
    setDefaultCoords(): void {
        this.setNewCoords(this.maxExtent);
    }

    setNewCoords(data: location): void {
        this.coordsBS$.next(data);
    }
    // setNewCoords(maxlat, minlat, maxlon, minlon): void {
        // this.coordsBS$.next({
            // maxlat,
            // minlat,
            // maxlon,
            // minlon
        // });
    // }

    displayInFilter():string{
        const coords = this.getLatestCoords();
        return !coords.is_geopoint ? `Longitude: ${this.utilService.round(coords.bbox.minlon)}, ${this.utilService.round(coords.bbox.maxlon)}, Latitude: ${this.utilService.round(coords.bbox.minlat)}, ${this.utilService.round(coords.bbox.maxlat)}`
            :
            `LAT: ${this.utilService.round(coords.point.lat)}, LON: ${this.utilService.round(coords.point.lon)}, Radius: ${coords.point.radius} ${coords.point.radiusUnit}`
            ;
    }
    isActive(): boolean {
        const coords = this.getLatestCoords();
        return !coords.hasOwnProperty('bbox') || !(coords.bbox.maxlat === this.maxExtent.bbox.maxlat
            && coords.bbox.minlat === this.maxExtent.bbox.minlat
            && coords.bbox.maxlon === this.maxExtent.bbox.maxlon
            && coords.bbox.minlon === this.maxExtent.bbox.minlon);
    }
    setMap(map:any) {
        this.map = map;
    }
    clear() {
        if (this.map && typeof this.map.clean === 'function'){
            this.map.clean();
        }
        this.setDefaultCoords();
    }
}

