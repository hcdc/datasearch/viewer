import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { Collapsible } from './collapsible';
import { DateRange } from '../typings';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, interval } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DateService extends Collapsible{

  private minDate = new Date(1900,0,1);
  private maxDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
  private outputDateFormat = 'YYYYMMDD';
  private inputDateFormat = "YYYY-MM-DD";
  public errormessagekey = 0;
  public isvaildStartDate = 0;
  public isvaildEndDate = 0;
  public readonly waitingResetTimeout = 10000;
  private timer_counter$: BehaviorSubject<Number> = new BehaviorSubject(this.waitingResetTimeout/1000);
  private timerSubscription: Subscription;
  public timeoutSubject;
  public timeRangeBS$: BehaviorSubject<DateRange> = new BehaviorSubject({
    start: this.minDate,
    end: this.maxDate
  });
  public readonly timeRange$:Observable<DateRange>  = this.timeRangeBS$;
  public readonly startDate$:Observable<Date>  = this.timeRangeBS$.pipe(map(range => range.start));
  public readonly endDate$:Observable<Date>  = this.timeRangeBS$.pipe(map(range => range.end));
  public isRangeInvalid$:Observable<boolean> = this.timeRangeBS$.pipe(map(range => this.isRangeValid(range)));

  constructor(private injector: Injector) { super(injector, 'date'); }
  isActive(): boolean {
    return (this.timeRangeBS$.value.start.getTime() !== this.minDate.getTime() ||
      this.timeRangeBS$.value.end.getTime() !== this.maxDate.getTime()) &&
      this.timeRangeBS$.value.start.getTime() <= this.timeRangeBS$.value.end.getTime()
    //return (this.timeRangeBS$.value.start.getTime() !== this.minDate.getTime())
    //|| (this.timeRangeBS$.value.end.getTime() !== this.maxDate.getTime());
  }
  displayInFilter(): string {
    return this.pretty(this.timeRangeBS$.value);
  }

  isRangeValid(range){
    const start: Date = range.start;
    const end: Date = range.end;
    const isValid = start <= end
      && start >= this.minDate
      && end <= this.maxDate;

    if (isValid) {
      clearTimeout(this.timeoutSubject);
      this.errormessagekey=0;
      this.isvaildStartDate = 0;
      this.isvaildEndDate = 0;
      if (this.timerSubscription) this.timerSubscription.unsubscribe();
    }
    return !isValid;
  }
  getLatestTimeRange() {
    return this.timeRangeBS$.value;
  }
  setNewStartDate(start) {
    this.timeRangeBS$.next({
      start,
      end: this.timeRangeBS$.value.end
    });
  }
  setNewEndDate(end) {
    this.timeRangeBS$.next({
      start: this.timeRangeBS$.value.start,
      end
    });
  }
  setNewTimeRange(start: Date, end: Date) {
    this.timeRangeBS$.next({
      start,
      end
    });
  }

  getErrorMessage(error_key){
    switch (error_key) {
      case 1:
        return `Start date is wrong, please use ${this.inputDateFormat} or it will be reset in ${this.timer_counter$.value} seconds.`  ;
        break;
      case 2:
        return `End date is wrong, please use ${this.inputDateFormat} or it will be reset in ${this.timer_counter$.value} seconds.`;
        break;
      case 3:
        return `The selected time range should be between ${moment(this.minDate).format('LL')} - ${moment(this.maxDate).format('LL')}, please choose another date or will it be reset in ${this.timer_counter$.value} seconds.`;
        break;
      case 4:
        return `Start date should be between ${moment(this.minDate).format('LL')} - ${moment(this.timeRangeBS$.value.end).format('LL')}, please choose another date or will it be reset in ${this.timer_counter$.value} seconds.`;
        break;
      case 5:
        return `End date should be between ${moment(this.timeRangeBS$.value.start).format('LL')} - ${moment(this.maxDate).format('LL')}, please choose another date or it will be reset in ${this.timer_counter$.value} seconds.`;
        break;
      default:
        return `The selected time range ${moment(this.timeRangeBS$.value.start).format('LL')} - ${moment(this.timeRangeBS$.value.end).format('LL')} is invalid, please choose another dates or the last modified one will be reset in ${this.timer_counter$.value} seconds.`;
        break;
    }

  }
  isvalidDateFormat(dateString, dateFormat = this.inputDateFormat){
    const m = moment(dateString, dateFormat, true);
    if (!m) {
      return null
    }
    return m.isValid();
  }
  dateInRange(dateString, dateFormat = this.inputDateFormat) {
    const d = moment(dateString, dateFormat, true);
    return d.isValid() && d >= moment(this.minDate, dateFormat, true) && d <= moment(this.maxDate, dateFormat, true);
  }
  getdatefromstr(dateString, dateFormat = this.inputDateFormat): NgbDateStruct   {
    const m = moment(dateString, dateFormat).toDate();
    if (!m) {
      return null;
    }
    return { day: m.getDate(), month: m.getMonth() + 1, year: m.getFullYear() }
  }

  getMinDate() {
    return this.minDate;
  }

  getMaxDate() {
    return this.maxDate;
  }
  pretty(timerange: DateRange) : string{
    return `${moment(timerange.start).format('LL')} - ${moment(timerange.end).format('LL')}`;
  }
  formatTimeRange(timerange: DateRange) {
    return {
      start: moment(timerange.start).format(this.outputDateFormat),
      end: moment(timerange.end).format(this.outputDateFormat),
    }
  }
  getIsPickerActive(): boolean {
    return (this.timeRangeBS$.value.start.getTime() !== this.minDate.getTime())
    || (this.timeRangeBS$.value.end.getTime() !== this.maxDate.getTime());
  }
  resetBoth() {
    this.reset(true);
    this.reset(false);
  }
  reset(isStartDate) {
    if(isStartDate){
      this.timeRangeBS$.next({
        start: this.minDate,
        end: this.timeRangeBS$.value.end
      });
    }else {
      this.timeRangeBS$.next({
        start: this.timeRangeBS$.value.start,
        end: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
      });
    }
  }
  today(isStartDate){
    if (isStartDate) {
      this.timeRangeBS$.next({
        start: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()),
        end: this.timeRangeBS$.value.end
      });
    } else {
      this.timeRangeBS$.next({
        start: this.timeRangeBS$.value.start,
        end: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate())
      });
    }
  }
  timer(seconds: number = this.waitingResetTimeout/1000 ){
    this.timer_counter$.next(seconds);
    if (this.timerSubscription) this.timerSubscription.unsubscribe();
    //this.timerSubscription = interval(1000)
    //  .subscribe(x => { this.timer_counter$.next(seconds-x); if (x > seconds) this.timerSubscription.unsubscribe(); });
    this.timerSubscription = interval(1000).pipe(take(seconds)).subscribe(x => this.timer_counter$.next(seconds - x -1));
  }
  update_maxDate(): boolean{
    const f = this.maxDate && new Date().getDate() !== this.maxDate.getDate();
    if (f) this.maxDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    return f;
  }
}