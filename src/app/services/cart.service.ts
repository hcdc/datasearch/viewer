import { UtilService } from './util.service';
import { Collapsible } from './collapsible';
import { Injectable, OnInit, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cart, CartItem, Hit } from '../typings';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class CartService extends Collapsible {
    defaultCart: Cart = {
        id: Date.now(),
        email: '',
        items: []
    }
    public msgToUser =  '';
    public loading = false;
    private disclaimer_checked = false;
    private timeout_request = 15000;
    public request_submited = false
    constructor(
        public utilService: UtilService,
        private injector: Injector
    ) { super(injector, 'cart'); }

    public cartBS$ = new BehaviorSubject<Cart>(this.defaultCart);

    disclaimer_check() {
        this.disclaimer_checked = !this.disclaimer_checked;
    }

    isCartValid(){
        return this.allITemsValid() && this.isActive();
    }
    allITemsValid(){
        let allValid = true;
        this.cartBS$.value.items.forEach( item => {
            const hit = item.hit;
            if (hit.downloadFormats
                && hit.downloadFormats.length > 0
                && ((hit.chosenFormats && hit.chosenFormats.length === 0) || !("chosenFormats" in hit))) {
                    allValid = false;
            }
        });
        return allValid;
    }
    isActive(): boolean {
        return this.cartBS$.value.items.length > 0 && !this.request_submited;
    }
    addItem(hit: Hit) {
        if (this.loading === false && (typeof hit.inCart === 'undefined' || !hit.inCart)) {
            this.open();
            const item = this.createCartItem(hit);
            const cart = this.cartBS$.value;
            // Make hide succes message of previous searches
            if (cart.items.length === 0) {
                this.msgToUser = '';
            }
            if (this.itemNotIncludedInCart(cart, item)) {
                cart.items.push(item);
                item.hit.inCart = true;
            }
            this.cartBS$.next(cart);
            this.activeControllCaption = ' ' + this.cartBS$.value.items.length + ' ';
        }

    }
    createCartItem(hit: Hit): CartItem {
        const cartItem = {
            id: this.utilService.getHash(hit),
            hit
        }
        return cartItem;
    }
    itemNotIncludedInCart(cart: Cart, item: CartItem): boolean {
        return !cart.items.map(i => i.id).includes(item.id);
    }
    removeItem(id: number) {
        if (this.loading === false) {
            const cart = this.cartBS$.value;
            cart.items.forEach(item => { if (item.id == id) item.hit.inCart = false; });
            const items = cart.items.filter(i => i.id !== id);
            if (items.length == 0) this.msgToUser = ``;
            cart.items = items;
            this.cartBS$.next(cart);
            this.activeControllCaption = ' ' + this.cartBS$.value.items.length + ' ';
        }
    }
    clear() {
        this.cartBS$.value.items = [];
    }
    buildRequest() {
        return {};
    }
    resetCart() {
        const cart_ = this.cartBS$.value;
        cart_.items.forEach(item => item.hit.inCart = false );
        this.cartBS$.next(cart_);
        const cart = this.defaultCart;
        cart.id = Date.now();
        cart.email = '';
        cart.items = [];
        this.cartBS$.next(cart);
    }
    submit(email) {
        this.msgToUser = ``;
        if(!this.isCartValid()){
            this.msgToUser = 'Please choose your download formats.';
        }else {
            const cart = this.cartBS$.value;
            cart.id = Date.now();
            const apiCart = this.prepareCartForBackend(cart, email);
            this.loading = true;
            this.request_submited = true;
            this.sendPostRequest(apiCart);

        }
    }
    sendPostRequest(apiCart) {
      this.utilService.sendTimeoutPostRequest(
          'download',
          apiCart,
          this.timeout_request,
          (err) => {
            this.loading = false;
            this.request_submited = false;
            this.handleResponse({
                status: 1,
                sent_email_status: 1
              },
              apiCart);
            throw new Error("timeout");

          }
        )
        .subscribe(
          (result: any) => {
            this.loading = false;
            this.handleResponse(result, apiCart);
            this.request_submited = false;
          },
          error => {
            this.request_submited = false;
            /*
            if (error.message == 'timeout') {
                console.log("timeout happend");
            }
            */
          }
        );
    }

    handleResponse(result, cart){
        this.msgToUser += `<div ><h3>Your order number: ${cart.CARTID}</h3></div>`;
        if(result && result.status && result.sent_email_status){
            if (result.download_url){
                this.msgToUser += `
                <div >${result.msg}, click <a href="${result.download_url}" target="_blank">here!</a> to download.<div>`;
                if (result.sent_email_status) {
                    this.msgToUser += `
                    <div>We also sent you an email contaning the download link.<div>`;
                }else{
                    this.msgToUser += `
                    <div>For an unknown reason we could not send you an email, please <a href="mailto:hcdc_support.hereon.de">contact us</a>.<div>`;
                }
            } else
            {
               this.msgToUser += `
                <div>You will receive an email containing your download instructions soon.<div>`;
            }

            this.resetCart();
        } else {
            this.msgToUser += `
            <div> An error prevented the collection of the data. please <a href="mailto:hcdc_support.hereon.de">contact us</a>.<div> \n<div>`;
        }


        if(result.problems && Array.isArray(result.problems) && result.problems.length > 0){
            result.problems.forEach(problem => {
                this.msgToUser = `${this.msgToUser}<br>${problem.problems.join('<br>')}`
            })
            // this.msgToUser = result.problems.join('\n');
        }
    }
    /**
     * Prepares the cart to be sent to backend
     * and equals the format the Backend send's to the downloadAPI
     * @param cart the cart datastructure as represented in the front end
     * @param email email user provided
     */
    prepareCartForBackend(cart: Cart, email: string): any {
        const reqBody = {
            CARTID: cart.id,
            EMAIL: email,
            CART: []
        }
        cart.items.forEach(item => {
            reqBody.CART.push(this.buildCartItem(item.hit))
        });
        return reqBody;
    }
    buildCartItem(hit) {
        const payload = hit.payload;
        const STARTDATE = (hit.meta['Start date']) ?
            moment(moment(hit.meta['Start date'], "YYYY-MM-DD", true)).format('YYYYMMDD')
            :
            undefined
            ;
        const ENDDATE = (hit.meta['End date']) ?
            moment(moment(hit.meta['End date'], "YYYY-MM-DD", true)).format('YYYYMMDD')
            :
            undefined
            ;

        let cartItem = {
            STARTDATE,
            ENDDATE,
            Coordinates: hit.meta.Coordinates,
            requested: hit.requested,
            ...payload,
            SOURCE: hit.meta.index
        };
        if(hit.downloadFormats && hit.downloadFormats.length > 0 && hit.chosenFormats && hit.chosenFormats.length > 0){
            cartItem['FORMAT'] = hit.chosenFormats;
        }
        cartItem = this.utilService.objKeysToUpper(cartItem);
        return cartItem;
    }
}