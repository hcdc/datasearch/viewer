import { Injectable, Injector, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Hit } from '../typings';
import { Collapsible } from './collapsible';
import { DateService } from './date.service';
import { FilterService } from './filter.service';
import { MapService } from './map.service';
import { UtilService } from './util.service';

@Injectable({
    providedIn: 'root'
})
export class ResultService extends Collapsible implements OnDestroy {
    public resultBS$ = new BehaviorSubject<Hit[]>([]);
    public readonly result$ = this.resultBS$.asObservable();
    filterSubscription: Subscription
    mapSubscription: Subscription;
    dateSubscription: Subscription;

    isActive(): boolean {
        return false;
    }

    constructor(
        private injector: Injector,
        public utilService: UtilService,
        public filterService: FilterService,
        public mapService: MapService,
        public dateService: DateService
    ) {
        super(injector, 'result');
        this.filterSubscription = filterService.filter$.subscribe(() => this.searchfields());
        this.mapSubscription = mapService.coords$.subscribe(() => this.searchfields());
        this.dateSubscription = dateService.timeRange$.subscribe(() => this.searchfields());
    }
    ngOnDestroy(): void {
        this.filterSubscription.unsubscribe();
        this.mapSubscription.unsubscribe();
        this.dateSubscription.unsubscribe();
    }
    searchfields(): void {
        const filter = this.filterService.filterBS$.value;
        const coords = (this.mapService.isActive()) ? this.mapService.coordsBS$.value : undefined;
        const timerange = (this.dateService.isActive()) ? this.dateService.formatTimeRange(this.dateService.timeRangeBS$.value) : undefined;

        if (filter && Array.isArray(filter) && filter.length > 0) {
            this.utilService.sendPostRequest('searchfields', { fields: filter, coords, timerange })
                .subscribe(result => {
                    if (typeof result === 'string') {
                        console.error(result);
                    } else {
                        this.resultBS$.next(this.sort_results(result) as Hit[]);
                    }
                });
        }
    }
    listValues(fields, filter, index): Observable<any> {
        const timerange = this.dateService.formatTimeRange(this.dateService.timeRangeBS$.value);
        const coords = this.mapService.coordsBS$.value;
        return this.utilService.sendPostRequest('listvalues', { fields, filter, index, coords, timerange });
    }

    sort_results= (obj) => {
        return obj.sort(function (a, b) {
            return (a.filter_length > b.filter_length) ? -1 : (a.filter_length < b.filter_length) ? 1 : 0;
        })
    }

}