import { FilterService } from './filter.service';
import { UtilService } from './util.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Suggestion, Suggest, TimeRange, location, FilterItem  } from '../typings';
import { DateService } from './date.service';
import { MapService } from './map.service';
@Injectable({
  providedIn: 'root'
})
export class SuggestService {
  public suggestions$ = new BehaviorSubject<Suggestion[]>([]);
  private suggest_template = new suggestion();
  constructor(
    public utilService: UtilService,
    public filterService: FilterService,
    public dateService: DateService,
    public mapService: MapService
    ) {
      // prevent circular dependency in dependency injection
      filterService.setSuggestService(this);
    }
  suggEquals(a, b){
    return a.type === b.type && a.input === b.input;
  }
  addSuggestion(sugg){
    const suggs = this.suggestions$.value;
    if(suggs.filter((s: Suggestion) => this.suggEquals(s, sugg)).length === 0){
      suggs.push(sugg);
      this.suggestions$.next(suggs);
    }
  }
  removeSuggestion(sugg: any) {
    // remove clicked suggestion from suggestions
    const suggs = this.suggestions$.value
    const suggsWithoutSugg = suggs.filter((s: Suggestion) => !this.suggEquals(s, sugg))
    this.suggestions$.next(suggsWithoutSugg);
  }
  suggest(term) {
    this.suggest_template.prefix = this.utilService.removeTags(term);
    if (this.suggest_template.prefix.length > 2) {
      this.suggest_template.filter = this.filterService.filterBS$.value;
      this.suggest_template.time = (this.dateService.isActive()) ? this.dateService.formatTimeRange(this.dateService.timeRangeBS$.value) : undefined;
      this.suggest_template.location = (this.mapService.isActive()) ? this.mapService.coordsBS$.value: undefined;
      this.utilService.sendPostRequest('suggest', this.suggest_template)
        .subscribe(
          value => {
            if (value) {
              this.utilService.addCSSClassToSuggestions(value);
              this.suggestions$.next(value);
            }
          });
    }
  }
}

class suggestion implements Suggest {
  prefix: string;
  time: TimeRange;
  location: location;
  filter: FilterItem[];
}