import { SuggestService } from './suggest.service';
import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { UtilService } from './util.service';
import { Collapsible } from './collapsible';
import { FilterItem } from '../typings';

@Injectable({
  providedIn: 'root'
})
export class FilterService extends Collapsible{

  public filterBS$ = new BehaviorSubject<FilterItem[]>([]);
  public readonly filter$ = this.filterBS$.asObservable();
  // nedded for manual dependency injection
  suggestService: SuggestService;

  setSuggestService(service: SuggestService) {
    this.suggestService = service;
  }
  constructor(
    public utilService: UtilService,
    private injector: Injector
  ) { super(injector, 'filter'); }

  getFilter() {
    return this.filterBS$.value;
  }
  setFilter(value: FilterItem[]) {
    value = this.utilService.addCSSClassToSuggestions(value);
    this.filterBS$.next(value);
  }
  addToFilter(sugg) {
    this.open();
    const filter = this.filterBS$.value;
    if (this.suggNotIncludedInFilter(filter, sugg)) {
      filter.push(sugg);
    }
    this.sortByType(filter);
    this.sort_by_input(filter);
    this.filterBS$.next(filter);
  }
  removeFilter(fieldToRemove) {
    let fields = this.filterBS$.value;

    fields = fields.filter(field => !(field.type === fieldToRemove.type && field.input === fieldToRemove.input));
    this.filterBS$.next(fields);
    this.suggestService.addSuggestion(fieldToRemove);
    // this.clearInputField();
  }
  suggNotIncludedInFilter(fields, sugg) {
    return !fields.map(field => [field.input, field.type]).some(row => JSON.stringify(row) === JSON.stringify([sugg.input, sugg.type]));
  }
  sortByType(filter) {
    return filter.sort((a, b) => {
      if (a.type > b.type) {
        return 1;
      } else {
        return -1;
      }
    });
  }
  sort_by_input = (ff) => {
    return ff.sort(function (a, b) {
      const textA = a.input.toLowerCase();
      const textB = b.input.toLowerCase();
      const typeA = a.type.toLowerCase();
      const typeB = b.type.toLowerCase();
      return (typeA == typeB && textA < textB) ? -1 : (typeA == typeB && textA > textB) ? 1 : 0;
    })
  }
  isActive(): boolean {
    return this.filterBS$.value.length > 0;
  }
  public clear(): void {
    this.filterBS$.next([]);
  }
}
