import { SuggestService } from './../services/suggest.service';
import { FilterService } from './../services/filter.service';
import { UtilService } from '../services/util.service';
import { Component, ViewChild, ElementRef, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { ResultService } from '../services/result.service';
import { fromEvent } from 'rxjs/internal/observable/fromEvent';
import { map } from 'rxjs/internal/operators/map';
import { merge } from 'rxjs/internal/operators/merge';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import { filter } from 'rxjs/internal/operators/filter';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements AfterViewInit, OnDestroy {
  isDropDownShown = true;
  inputFormControl = new FormControl('');
  focus_index = 0;
  @Input() tooltiptext: string = "";
  coords$: Observable<any>;
  sub$ = new Subject<string>();
  @ViewChild('search')
  searchInput: ElementRef;
  @ViewChild('dropdown')
  dropdownList: ElementRef;
  keyupEnterSubscription: Subscription;
  keyupSubscription: Subscription;
  constructor(
    private utilService: UtilService,
    public resultService: ResultService,
    public filterservice: FilterService,
    public suggestService: SuggestService) { }
  ngAfterViewInit(): void {

    // not Enter key
    this.keyupSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      filter((event: any) => event.key !== 'Enter'),
      map((event: any) => {
        return event.target.value;
      })
      // Time in milliseconds between key events
      , debounceTime(300)
      // If previous query is diffent from current
      , distinctUntilChanged()
    ).subscribe({ next: (prefix: string) => this.handleKeyup(prefix) });

//    // Enter key
//    this.keyupEnterSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
//      filter((event: any) => event.key === 'Enter' && this.isDropDownShown)
//      // If previous query is diffent from current
//      , distinctUntilChanged()
//    ).subscribe({ next: (ignore: any) => this.handleEnter() });




    const keyDowns = fromEvent(this.searchInput.nativeElement, 'keydown');
    const keyUps = fromEvent(this.searchInput.nativeElement, 'keyup');
    const keyPresses = keyDowns.pipe(
      merge(keyUps),
      filter((e: KeyboardEvent) => [38, 40, 13].includes(e.keyCode) ),
      distinctUntilChanged((x, y) => x.type == y.type)
    );

    keyPresses.subscribe((ctrlpress) => {
      if (ctrlpress.type === 'keyup') {
        this.activeKeyboardArrow(ctrlpress.keyCode);
      }
    });
  }
  change(item){
    const id = item.target.id.split('-')
    this.focus_index = (id.length) ? Number(id[1]) : this.focus_index;
    this.enableHover();
  }
  disableHover(){
    if (this.isDropDownShown){
      if (this.focus_index >= 0 && this.focus_index < this.suggestService.suggestions$.value.length) {
        this.dropdownList.nativeElement.getElementsByTagName("li")[this.focus_index].classList.add("suggest_arrow");
      }
    }
  }
  focus(){
    this.isDropDownShown = true;
  }
  enableHover(){
    if (this.isDropDownShown) {
      Array.prototype.forEach.call(this.dropdownList.nativeElement.getElementsByTagName("li"), element => {
        element.classList.remove("suggest_arrow");
      });
      // this.dropdownList.nativeElement.getElementsByTagName("li").forEach(element => {
      //   element.classList.remove("suggest_arrow");
      // });
      this.dropdownList.nativeElement.getElementsByTagName("li")[this.focus_index].classList.add("suggest_arrow");
    }
  }
  activeKeyboardArrow(code: number = 13) {
    let go;
    if (this.isDropDownShown && this.suggestService.suggestions$.value.length > 0) {
      if (this.focus_index >= 0 && this.focus_index < this.suggestService.suggestions$.value.length) {
        this.dropdownList.nativeElement.getElementsByTagName("li")[this.focus_index].classList.remove("suggest_arrow", "disabled");
        // this.dropdownList.nativeElement.getElementsByTagName("li")[this.focus_index].scrollIntoView(true);
        // this.dropdownList.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: 'nearest' });
      }
      this.focus_index = (code === 40) ? (this.focus_index += 1) : ((code === 38) ? (this.focus_index -= 1) : (this.focus_index));
      if (this.focus_index == -1 && code === 13) {
        go = false;
      } else {
        go = true;
        this.focus_index = (this.focus_index < 0) ? (this.focus_index = this.suggestService.suggestions$.value.length - 1) : ((this.focus_index >= this.suggestService.suggestions$.value.length) ? (this.focus_index = 0) : (this.focus_index));
      }
      if (go) {
        this.dropdownList.nativeElement.getElementsByTagName("li")[this.focus_index].scrollIntoView({ behavior: 'smooth', block: 'end' });
        if (code === 38 || code === 40) {
          this.disableHover();
        } else {
          this.enableHover();
        }
        if (code === 13) {
          this.handleEnter(this.focus_index);
        }
      }


      }
  }

  ngOnDestroy(): void {
    this.keyupEnterSubscription.unsubscribe();
    this.keyupSubscription.unsubscribe();
  }

  handlePaste(event) {
    const clipboardData = event.clipboardData;
    const pastedText = clipboardData.getData('text');
    this.suggestService.suggest(pastedText);
  }
  handleEnter(idx: number = 0) {
    if (this.suggestService.suggestions$.value.length > 0 && this.isDropDownShown === true) {
      this.handleSuggestionClick(this.suggestService.suggestions$.value[idx]);
    }
  }
  handleKeyup(prefix) {
    prefix = prefix.trim();
    this.isDropDownShown = true;
    this.suggestService.suggest(prefix);
    this.focus_index = -1;
  }

  handleSuggestionClick(sugg) {
    sugg.input = this.utilService.removeTags(sugg.input)
    this.suggestService.removeSuggestion(sugg);
    this.isDropDownShown = false;
    this.clearInputField();
    this.filterservice.addToFilter(sugg);
  }


  clearInputField() {
    this.searchInput.nativeElement.value = '';
  }

  showDropDown() {
    this.isDropDownShown = true;
    this.focus_index = -1;
  }
}

