# download-frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.0.

After cloning run:
```bash
npm install @angular/cli -g

npm install
```


## Development server

- Go to **`src/environments`** folder then fill the `environment.ts`  regarding to your local environment
- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
- please go to **`src/environments`** folder then fill the `environment.prod.ts` and `environment.staging.ts`  regarding to your internal environment
- Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
- staging: `ng build --configuration staging --base-href=.`
- production `ng build --configuration production --base-href=.`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Copyright

Copyright © 2023 Helmholtz-Zentrum Hereon

Licensed under the EUPL-1.2-or-later

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the EUPL-1.2 license for more details.

You should have received a copy of the EUPL-1.2 license along with this
program. If not, see https://www.eupl.eu/.
